![Privacy aspects in Volunteered Geographic Information](_images/header.jpg)

Social media data is widely used to gain insights about social incidents, whether on a local or global scale.
Within the process, it is common practice to download and store it locally, while privacy considerations are often neglected.
However, protecting the privacy of social media users is demanded by laws and ethics.
In order to prevent subsequent abuse, theft or public exposure of collected datasets, privacy-aware data processing is crucial.

In this project we developed a data storage concept that enables to process social media data with social media user's privacy in mind.
Based on the [cardinality estimator HyperLogLog (HLL)][flj], the concept enables to store social media data, so that it is not possible to extract individual items from it.
You can only estimate the cardinality of items within a certain set and run set operations over multiple sets to extend analytical ranges.
However, applying this method requires to define the scope of the result before even gathering the data.
This prevents the data from being misused for other purposes at a later point in time and thus follows the [privacy by design principles][pbd].

[flj]: http://algo.inria.fr/flajolet/Publications/FlFuGaMe07.pdf
[pbd]: https://www.ipc.on.ca/wp-content/uploads/resources/7foundationalprinciples.pdf

![](_images/sink-graphic.svg)  
*Social media data processing graph. (a) Example post. (b) The post’s social, temporal, spatial (green), and topical data, and its hidden unique ID (red). (c) Encode the corresponding geohash from the geo-coordinates. The result represents the area plotted by the rectangle over the outlines of Dresden. (d) Store the post ID in the HLL set of the database record matching the geohash.*

* ![][pub] [Löchner and Burghardt, 2022](https://doi.org/10.3390/ijgi12020060)
* ![][api] [vgisink.vgiscience.org](https://vgisink.vgiscience.org)

## Implementations

* Proof-of-concept implementation to store social media data using the cardinality estimator HyperLogLog

    ![][glg] [ml/vgisink]

* Containerized database environment for the [VGIsink][ml/vgisink] project

    ![][glg] [ml/sinkdb]

* Leaflet application for [VGIsink][ml/vgisink] location cardinalities

    ![][glg] [ml/sinkmap]

[ml/vgisink]: https://gitlab.vgiscience.de/ml/vgisink
[ml/sinkdb]: https://gitlab.vgiscience.de/ml/sinkdb
[ml/sinkmap]: https://gitlab.vgiscience.de/ml/sinkmap

## Collaborations

The [EVA-VGI] project studies quality, heterogeneity, subjectivity, spatial resolution and temporal relevance of geo-referenced social media data.
Focusing on the integration of spatial, temporal, topical and social dimensions combined with an explicit link between events and reactions, the project
presents a number of conceptual approaches and methods that enable a privacy-aware visual analysis of VGI in general and geo social media data in particular.
The project has taken advantage of the results of our research by implementing HLL on datasets related to their publications.

* ![][pub] [Dunkel et al., 2020](https://doi.org/10.3390/ijgi9100607)
* ![][vid] [Visualizing opinions, emotions and perceptions in social media data - while preserving user privacy](https://www.vgiscience.org/2022/12/21/visualizing-opinions-emotions-and-perceptions-in-social-media-data-while-preserving-user-s-privacy.html)

Together with the [DVCHA] and [VA4VGI] projects we formed a [Young Research Group] and carried out a case study, wherein we explored the deployment of HLL into disaster management processes.
We developed and conducted a focus group discussion with VOST members, where we identified challenges and opportunities of working with HLL and compared the process with conventional techniques.
Findings showed that deploying HLL in the data acquisition process of VOST operations will not distract their data analysis process.
Instead, several benefits, such as improved working with huge datasets, may contribute to a more widespread use and adoption of the presented technique, which provides a basis for a better integration of privacy considerations in disaster management.

* ![][www] [Privacy-Aware Social Media Data Processing in Disaster Management](https://www.vgiscience.org/yrg/pridis.html)
* ![][pub] [Löchner et al., 2020](https://doi.org/10.3390/ijgi9120709)

[EVA-VGI]: https://www.vgiscience.org/projects/eva-vgi-2.html
[DVCHA]: https://www.vgiscience.org/projects/dvcha-2.html
[VA4VGI]: https://www.vgiscience.org/projects/va4vgi-2.html 
[Young Research Group]: https://www.vgiscience.org/yrg/pridis.html

## Conferences

* [Collaborative Research Week, Heidelberg, 2018](https://hcrw.vgiscience.org)

  ![][www] [LBSN Structure](https://lbsn.vgiscience.org/structure/)

* [VGI Geovisual Analytics Workshop at BVDA, Konstanz, 2018](https://www.vgiscience.org/vgi-geovisual-analytics-workshop.html)

  ![][pub] [Löchner et al., 2018](https://ml.vgiscience.org/downloads/loechner2018abstraction.pdf)

* [1st Int. Workshop on Legal and Ethical Issues in Crowdsourced Geographic Information, Zurich, 2019](https://web.archive.org/web/20230301123341/https://www.cs.nuim.ie/~pmooney/lesson2019)

  ![][pub] [Löchner et al., 2019](https://ml.vgiscience.org/downloads/loechner2019lesson.pdf)

* [VGIscience Lecture Series, online, 2021](https://www.vgiscience.org/lecture-series.html)

  ![][vid] [Privacy Aspects in VGI](https://downloads.vgiscience.org/lecture-series/privacy-aspects/privacy-aspects.html)

* [VGI and Smart Cities Workshop at SCCON, Berlin, 2022](https://www.vgiscience.org/2022/10/20/sccon.html)

  ![][pub] [Poster](https://ml.vgiscience.org/downloads/loechner2022sccon.pdf)

[glg]: https://www.vgiscience.org/assets/images/logo-gitlab-grayscale.svg
[pub]: https://www.vgiscience.org/assets/images/dummy-publication.svg
[vid]: https://www.vgiscience.org/assets/images/dummy-video.svg
[www]: https://www.vgiscience.org/assets/images/dummy-www.svg
[api]: https://www.vgiscience.org/assets/images/api-url.svg
